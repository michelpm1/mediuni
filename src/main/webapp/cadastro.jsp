<!DOCTYPE html>
<html>
<title>MediUni Cadastro</title>

<body>
<%@ include file="header.jsp" %>
<container>
	<div class ="row">
		<div class = "col-md-4"></div>
	    <div class = "col-md-4">
	    <h1 class="tittle-register">Cadastro</h1>
	    <h4 class ="tittle-choose-pacient">Escolha o tipo de Paciente</h4>
	  
	  <select id="pacient-selection" class="form-control">
        <option value="mae">M�e</option>
        <option value="bebe">Beb�</option>
      </select>
	<form name="input" id="insert-mae" action="inserir_mae" method="post">
		Nome Completo: <input class="form-control" placeholder="Nome Completo" type="text" name="nome">
		CPF: <input class="form-control" placeholder="CPF" type="text" name="cpf">
		Conv�nio: <input class="form-control" placeholder="Nome Completo" type="text" name="convenio">
		Data de Nacimento: <input class="form-control" placeholder="Data" type="text" name="data">
		 Tipo do Sangue: <select id="" name= "tiposangue" class="form-control">
        <option value="O+">O+</option>
        <option value="O-">O-</option>
        <option value="A+">A+</option>
        <option value="A-">A-</option>
        <option value="B+">B+</option>
        <option value="B-">B-</option>
        <option value="AB+">AB+</option>
        <option value="AB-">AB-</option>
      </select>
		<input id="btn-submit" class="btn btn-success" type="submit" value="Enviar">
	</form>


	<form name="input" id="insert-bebe" action="inserir_bebe" method="post">
		Nome Completo: <input class="form-control" placeholder="Nome Completo" type="text" name="nome">
		Convenio: <input class="form-control" placeholder="Nome Completo" type="text" name="convenio">
		Data de Nacimento: <input class="form-control" placeholder="Data" type="text" name="data">
		CPF da M�e: <input class="form-control" placeholder="CPF" type="text" name="cpf-mae">
		Tipo do Sangue: <select id="" name= "tiposangue" class="form-control">
        <option value="O+">O+</option>
        <option value="O-">O-</option>
        <option value="A+">A+</option>
        <option value="A-">A-</option>
        <option value="B+">B+</option>
        <option value="B-">B-</option>
        <option value="AB+">AB+</option>
        <option value="AB-">AB-</option>
        </select>
		<input id ="btn-submit" class="btn btn-success" type="submit"value="Enviar">
	</form>	
	</div>
	<div class = "col-md-4">
	<div class="css-alert-insert" id="showAlert">Cadastro Realizado!</div>
		<div class="css-alert-insert" id="showAlert1">Digite a data neste formato: YYYY-MM-DD</div>
		<div class="css-alert-insert" id="showAlert2">CPF inv�dio</div>
	</div>
</container>
<%@ include file="footer.jsp" %>
</body>
<script type="text/javascript">
$('#pacient-selection').change(function () {
	if ($('#pacient-selection option:selected').text() == "M�e"){
        $('#insert-mae').fadeIn();
        $('#insert-bebe').fadeOut();
        }
    else 
	{
        $('#insert-bebe').fadeIn();
    	$('#insert-mae').fadeOut();
    }
});


window.onload = function() {
	var aux = "";
	aux = '<%= request.getAttribute("auxForStatus") %>';
//	alert(aux);
	if (aux == "cheio") 
	{
        $('#showAlert').fadeIn();
        $('#showAlert1').fadeOut();
        $('#showAlert2').fadeOut();
        }
    else if  (aux == "formatodata")
    {
    	$('#showAlert').fadeOut();
        $('#showAlert1').fadeIn();
    	$('#showAlert2').fadeOut();
    }else if  (aux == "cpfinvalido")
    {
    	$('#showAlert').fadeOut();
        $('#showAlert1').fadeOut();
    	$('#showAlert2').fadeIn();
    }else{
    	

    }
};


</script>
</html>