package ultimosnascimentosServlet;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.bebe.Bebe;
import br.com.paciente.PacienteDao;
@WebServlet("/ultimos_nascimentos")
public class ultimosnascimentosServlet extends HttpServlet {

private static final long serialVersionUID = 1L;
public ultimosnascimentosServlet() { 
super(); 
} 
@Override
protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
{ 
	doPost(request, response);
} 

protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		

PacienteDao dao = new PacienteDao();
ArrayList<Bebe> filhoArray = new ArrayList<Bebe>();
filhoArray = dao.consultarUltimos();
String aux = "1";	
request.setAttribute("filho", filhoArray);
request.setAttribute("checker", aux);
//getServletConfig().getServletContext().getRequestDispatcher("/home.jsp").forward(request,response);
RequestDispatcher view = getServletContext().getRequestDispatcher("/home.jsp"); 
	view.forward(request,response); 
}

}
