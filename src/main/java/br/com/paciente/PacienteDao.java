package br.com.paciente;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import br.com.bebe.Bebe;
import br.com.factory.Factory;
import br.com.mae.Mae;

public class PacienteDao {



	public void inserir_paciente(Paciente paciente) {
		
		
		try{	
		new Factory();
		Connection con = Factory.getConexaoMySQL();

	    // cria um preparedStatement
	    String sql = "insert into paciente" +
	            " (nome, datanascimento, tiposangue, convenio, cpfmae, cpf)" +
	            " values (?,?,?,?,?,?)";
	    PreparedStatement stmt = con.prepareStatement(sql);
       //System.out.println(cidade.getCidade_nome());
	    // preenche os valores
	  stmt.setString(1, paciente.getNome());
	  stmt.setString(2, paciente.getData());
	  stmt.setString(3, paciente.getTipo_sangue());
	  stmt.setString(4, paciente.getConvenio());
	  if (paciente instanceof Bebe){
			Mae B = new Mae();
	 B = ((Bebe) paciente).getMae();
	 stmt.setString(5, B.getCpf());
	  } else{
		  stmt.setNull(5, java.sql.Types.INTEGER);
	  }
	  if (paciente instanceof Mae){
	  stmt.setString(6, ((Mae) paciente).getCpf());
	  } else{
		// stmt.setInt(6, (Integer) 0);
		  stmt.setNull(6, java.sql.Types.INTEGER);
		  
	  }
	  
	  

		    // executa
	  	stmt.execute();
	    stmt.close();
	    con.close();
				} catch(SQLException e){
					
					System.out.println("Não foi possível Gravar os dados no banco contate o administrador");
				}
		 if (paciente.getNome() != null){
			 System.out.println("Paciente inserido com sucesso!");
		 }
		    
	}
	public ArrayList<Bebe> consultar_cpf(Mae mae) {
		ArrayList<Bebe> filhoArray = new ArrayList<Bebe>();
		
		
		Bebe B = new Bebe();
	try{	new Factory();
	Connection con = Factory.getConexaoMySQL();

	String sql = "select idpaciente, datanascimento, tiposangue, convenio, nome from paciente where cpfmae = ?";

	java.sql.PreparedStatement preparedStatement = con.prepareStatement(sql);
	preparedStatement.setString(1, mae.getCpf());

	// execute select SQL stetement
	ResultSet rs = preparedStatement.executeQuery();	


	// executa o sql
	

	while (rs.next()) {
		Bebe Bebeaux = new Bebe();
		     
		 Bebeaux.setPaciente_id((rs.getInt("idpaciente")));
		 Bebeaux.setDataNascimento(rs.getString(("datanascimento")));
		 Bebeaux.setTipo_sangue((rs.getString("tiposangue")));
		 Bebeaux.setConvenio(rs.getString("convenio"));
		 Bebeaux.setNome(rs.getString("nome"));
		
		 
		 filhoArray.add(Bebeaux);
		 
		 
		
	
	}


con.close();
	} catch(SQLException e){
		System.out.println("CPF não encontrato tente novamente");
	 
	}


return filhoArray;
	}
	
	
	public Mae consultar_cpfNome(Mae mae) {
		Mae maeaux = new Mae();
		try{	new Factory();
		Connection con = Factory.getConexaoMySQL();

		String sql = "select idpaciente, datanascimento, tiposangue, convenio, cpf, nome from paciente where nome = ?";

		java.sql.PreparedStatement preparedStatement = con.prepareStatement(sql);
		preparedStatement.setString(1, mae.getNome());

		// execute select SQL stetement
		ResultSet rs = preparedStatement.executeQuery();
		
		while (rs.next()) {
			
			     
			 maeaux.setPaciente_id((rs.getInt("idpaciente")));
			 maeaux.setDataNascimento(rs.getString(("datanascimento")));
			 maeaux.setTipo_sangue((rs.getString("tiposangue")));
			 maeaux.setConvenio(rs.getString("convenio"));
			 maeaux.setCpf(rs.getString("cpf"));
			 maeaux.setNome(rs.getString("nome"));
		}
		con.close();
		} catch(SQLException e){
			System.out.println("Nome não encontrato tente novamente");
		 
		}


	return maeaux;
	} 
	
	public ArrayList<Bebe> consultarUltimos () {
		ArrayList<Bebe> filhoArray = new ArrayList<Bebe>();
		
		
		Bebe B = new Bebe();
	try{	new Factory();
	Connection con = Factory.getConexaoMySQL();

	String sql = "SELECT * FROM paciente WHERE cpf IS NULL and idpaciente ORDER BY idpaciente DESC LIMIT 5";

	java.sql.PreparedStatement preparedStatement = con.prepareStatement(sql);


	// execute select SQL stetement
	ResultSet rs = preparedStatement.executeQuery();	


	// executa o sql
	

	while (rs.next()) {
		Mae Maeaux = new Mae();
		Bebe Bebeaux = new Bebe();
		    
		 Bebeaux.setPaciente_id((rs.getInt("idpaciente")));
		 Bebeaux.setDataNascimento(rs.getString(("datanascimento")));
		 Bebeaux.setTipo_sangue((rs.getString("tiposangue")));
		 Bebeaux.setConvenio(rs.getString("convenio"));
		 Maeaux.setCpf(rs.getString("cpfmae")); 
		Bebeaux.setNome(rs.getString("nome"));
		Bebeaux.setMae(Maeaux);
		
		 
		 filhoArray.add(Bebeaux);
		 //Maeaux=null;
		 
		
	
	}


con.close();
	} catch(SQLException e){
		System.out.println("CPF não encontrato tente novamente");
		
	 
	}
	System.out.println("últimos 5 bebes nascidos na tela");

return filhoArray;
	}
}
