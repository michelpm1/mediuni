package br.com.servlets;

	import java.io.IOException; import java.io.PrintWriter; 


//import javax.servlet.jsp.*;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet; 
import javax.servlet.http.HttpServletRequest; 
import javax.servlet.http.HttpServletResponse; 
import javax.swing.JOptionPane;

import br.com.Validators.cpfValidator;
import br.com.Validators.dateValidator;
import br.com.bebe.Bebe;
import br.com.mae.Mae;
import br.com.mae.MaeDao;
import br.com.paciente.PacienteDao;
	@WebServlet("/inserir_mae") 
	public class MaeServlet extends HttpServlet { 
		private static final long serialVersionUID = 1L;
	//	private String cpfaux;
		public MaeServlet() { 
			super(); 
			} 
		protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
		{ 
			doPost(request, response);
		} 
		protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException { 
			dateValidator validDate = new dateValidator();
			String auxForStatus = "cheio";
			try {
				validDate.validade(request.getParameter("data"), response);
			} catch (Exception e) {
				auxForStatus = "formatodata";
				PrintWriter out = response.getWriter();
		        //out.write("invalid input");
				request.setAttribute("auxForStatus", auxForStatus );
				getServletContext().getRequestDispatcher("/cadastro.jsp").forward(request, response);
			  return;
			}
			
			//cpfaux = request.getParameter("cpf");
			cpfValidator validCpf = new cpfValidator();
			try {
				validCpf.validade(request.getParameter("cpf"), response);
			} catch (Exception e) {
				auxForStatus = "cpfinvalido";
				PrintWriter out = response.getWriter();
		        //out.write("invalid input");
			request.setAttribute("auxForStatus", auxForStatus );
			getServletContext().getRequestDispatcher("/cadastro.jsp").forward(request, response);
			  return;
			}
			
			//int cpfaux1=Integer.parseInt(cpfaux);
			Mae maenova = new Mae();
			maenova.setNome(request.getParameter("nome")); 
			maenova.setData(request.getParameter("data")); 
			maenova.setTipo_sangue(request.getParameter("tiposangue")); 
			maenova.setConvenio(request.getParameter("convenio")); 
			maenova.setCpf(request.getParameter("cpf")); 
		//	bebenovo.setMae(request.getParameter(mae));
		
			
			
			PacienteDao dao = new PacienteDao();
			dao.inserir_paciente(maenova);
			request.setAttribute("auxForStatus", auxForStatus );
			getServletConfig().getServletContext().getRequestDispatcher("/cadastro.jsp").forward(request,response);
			
			}
		}

	
