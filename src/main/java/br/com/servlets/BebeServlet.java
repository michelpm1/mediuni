package br.com.servlets;

	import java.io.IOException; import java.io.PrintWriter; 

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet; 
import javax.servlet.http.HttpServletRequest; 
import javax.servlet.http.HttpServletResponse; 
import javax.swing.JOptionPane;

import br.com.Validators.cpfValidator;
import br.com.Validators.dateValidator;
import br.com.bebe.Bebe;
import br.com.bebe.BebeDao;
import br.com.mae.Mae;
import br.com.mae.MaeDao;
import br.com.paciente.PacienteDao;
	@WebServlet("/inserir_bebe") 
	public class BebeServlet extends HttpServlet { 
		private static final long serialVersionUID = 1L;
		public BebeServlet() { 
			super(); 
			} 
		protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
		{ 
			doPost(request, response);
		} 
		protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			String auxForStatus = "cheio";
			dateValidator validDate = new dateValidator();
			try {
				validDate.validade(request.getParameter("data"), response);
			} catch (Exception e) {
			 auxForStatus = "formatodata";
				PrintWriter out = response.getWriter();
		        //out.write("invalid input");
				request.setAttribute("auxForStatus", auxForStatus );
				getServletContext().getRequestDispatcher("/cadastro.jsp").forward(request, response);
				return;
			
			}
			cpfValidator validCpf = new cpfValidator();
			try {
				validCpf.validade(request.getParameter("cpf-mae"), response);
			} catch (Exception e) {
			    auxForStatus = "cpfinvalido";
				PrintWriter out = response.getWriter();
		        //out.write("invalid input");
				request.setAttribute("auxForStatus", auxForStatus );
				getServletContext().getRequestDispatcher("/cadastro.jsp").forward(request, response);
			  return;
			}
			//System.out.println("ok");
			Bebe bebenovo = new Bebe();
			bebenovo.setNome(request.getParameter("nome")); 
			bebenovo.setData(request.getParameter("data")); 
			bebenovo.setTipo_sangue(request.getParameter("tiposangue")); 
			bebenovo.setConvenio(request.getParameter("convenio")); 
		    Mae maeinsert = new Mae();
		    maeinsert.setCpf((request.getParameter("cpf-mae")));
			bebenovo.setMae(maeinsert);
		
	
			
			PacienteDao dao = new PacienteDao();
			dao.inserir_paciente(bebenovo);
			request.setAttribute("auxForStatus", auxForStatus );
			getServletConfig().getServletContext().getRequestDispatcher("/cadastro.jsp").forward(request,response);
			
			}
		}

	
