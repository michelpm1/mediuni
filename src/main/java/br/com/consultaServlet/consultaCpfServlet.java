package br.com.consultaServlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.bebe.Bebe;
import br.com.mae.Mae;
import br.com.paciente.PacienteDao;

import java.util.ArrayList;

import javax.servlet.annotation.WebServlet;
import javax.swing.JOptionPane;
@WebServlet("/consultar_cpf") 
public class consultaCpfServlet extends HttpServlet{
	private static final long serialVersionUID = 1L;
	public consultaCpfServlet() { 
		super(); 
		} 
	//@Override
//	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	//{ 
//		doPost(request, response);
	//} 
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	    String checkConsulta = "1";
		String auxForStatus = "cheio";
	    Mae mae = new Mae();
	    mae.setCpf(request.getParameter("cpfmae"));

	
		
		PacienteDao dao = new PacienteDao();
		
		ArrayList<Bebe> filhoArray = new ArrayList<Bebe>();
	filhoArray = dao.consultar_cpf(mae);
	if (filhoArray.isEmpty()){
		auxForStatus = "vazio";
		
	}
	request.setAttribute("filho", filhoArray);
    request.setAttribute( "var", checkConsulta );
    request.setAttribute("auxForStatus", auxForStatus );
	//RequestDispatcher view = getServletContext().getRequestDispatcher("/consulta.jsp"); 
	//view.forward(request,response); 
	getServletConfig().getServletContext().getRequestDispatcher("/consulta.jsp").forward(request,response);
		}
}
