package br.com.factory;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import com.mysql.jdbc.PreparedStatement;
import com.mysql.jdbc.Statement;

	

public class Factory {


public static String status = "Não conectou...";

//Método Construtor da Classe//

   public Factory() {

}



//Método de Conexão//

public static Connection getConexaoMySQL() {

   Connection connection = null;          //atributo do tipo Connection



try {

//Carregando o JDBC Driver padrão

String driverName = "com.mysql.jdbc.Driver";                        

Class.forName(driverName);



//Configurando a nossa conexão com um banco de dados//
// NAO MUDAR AS INFS DO DB SE NAO, NAO IRA FUNCIONAR ONLINE, IGORAR O FACTORY SEMPRE QUE FOR UPAR ALGO

     //  String serverName = "127.11.58.2:3306";    //caminho do servidor do BD online
       String serverName = "localhost:8889";    //caminho do servidor do BD local
       String mydatabase = "maternidade";        //nome do seu banco de dados

       String url = "jdbc:mysql://" + serverName + "/" + mydatabase;

      // String username = "adminTwVZBWi";        //nome de um usuário de seu BD  online    

     //  String password = "t_e4VdawW8er";      //sua senha de acesso online
       
       String username = "root";        //nome de um usuário de seu BD      

      String password = "root";      //sua senha de acesso

       connection = DriverManager.getConnection(url, username, password);



       //Testa sua conexão//  

       if (connection != null) {

           status = ("STATUS--->Conectado com sucesso!");

       } else {

           status = ("STATUS--->Não foi possivel realizar conexão");

       }



       return connection;



   } catch (ClassNotFoundException e) {  //Driver não encontrado



       System.out.println("O driver expecificado nao foi encontrado.1");

       return null;

   } catch (SQLException e) {

//Não conseguindo se conectar ao banco

       System.out.println("Nao foi possivel conectar ao Banco de Dados.");

       return null;

   }



}



//Método que retorna o status da sua conexão//

public static String statusConection() {

   return status;

}



//Método que fecha sua conexão//

public static boolean FecharConexao() {

   try {

       Factory.getConexaoMySQL().close();

       return true;

   } catch (SQLException e) {

       return false;

   }



}



//Método que reinicia sua conexão//

public static java.sql.Connection ReiniciarConexao() {

   FecharConexao();



   return Factory.getConexaoMySQL();

}






}
